//
//  SearchCityStore.swift
//  SearchCity
//
//  Created by Adinan Yujaroen on 16/4/2565 BE.
//

import Foundation

protocol SearchCityStoreProtocol {
    func loadJson(completion: @escaping (Result<[Cities], ResponseError>) -> Void)
}

class SearchCityStore: SearchCityStoreProtocol {
    // load json file
    func loadJson(completion: @escaping (Result<[Cities], ResponseError>) -> Void) {
        if let url = Bundle.main.url(forResource: "cities", withExtension: "json") {
            do {
                let data = try Data(contentsOf: url)
                let decoder = JSONDecoder()
                let jsonData = try decoder.decode([Cities].self, from: data)
                completion(.success(jsonData))
            } catch {
                print("error:\(error)")
                completion(.failure(.invalidJSON))
            }
        } else {
            completion(.failure(.jsonFileNotFound))
        }
    }
}
