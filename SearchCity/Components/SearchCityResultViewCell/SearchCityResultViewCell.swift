//
//  SearchCityResultViewCell.swift
//  SearchCity
//
//  Created by Adinan Yujaroen on 17/4/2565 BE.
//

import UIKit

class SearchCityResultViewCell: UITableViewCell {
    static let identifier = "SearchCityResultViewCell"
    
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var subTitleLabel: UILabel!
    
    var city: Cities?
    
    override func setSelected(_ selected: Bool, animated: Bool) {
      super.setSelected(selected, animated: animated)
    }
    
    func updateUI(city: Cities) {
        self.city = city
        titleLabel.text = "\(city.name), \(city.country)"
        subTitleLabel.text = "\(city.coord.lat), \(city.coord.lon)"
    }
}
