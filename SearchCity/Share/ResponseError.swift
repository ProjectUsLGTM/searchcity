//
//  ResponseError.swift
//  SearchCity
//
//  Created by Adinan Yujaroen on 16/4/2565 BE.
//

import Foundation

enum ResponseError: Error {
    case invalidJSON
    case jsonFileNotFound
    case manageDataError
}
