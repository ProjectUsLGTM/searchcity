//
//  ShareService.swift
//  SearchCity
//
//  Created by Adinan Yujaroen on 16/4/2565 BE.
//

import Foundation
import UIKit

class ShareService {
    private static var sharedService: ShareService = {
        let sharedService = ShareService()
        return sharedService
    }()
    
    private init() {}
    
    class func shared() -> ShareService {
        return sharedService
    }
    
    private var vSpinner: UIView?
    
    func showLoader(view: UIView) {
        let spinnerView = UIView.init(frame: view.bounds)
        spinnerView.backgroundColor = UIColor.clear
        
        let loadingView: UIView = UIView()
        loadingView.frame = CGRect(x: 0, y: 0, width: 60, height: 60)
        loadingView.center = spinnerView.center
        loadingView.backgroundColor = UIColor.gray.withAlphaComponent(0.5)
        loadingView.layer.cornerRadius = 10
        
        let activityIndicator = UIActivityIndicatorView(style: .large)
        activityIndicator.center = CGPoint(x: loadingView.frame.size.width / 2, y: loadingView.frame.size.height / 2)
        
        loadingView.addSubview(activityIndicator)
        
        DispatchQueue.main.async {
            spinnerView.addSubview(loadingView)
            activityIndicator.startAnimating()
            view.addSubview(spinnerView)
        }
        
        vSpinner = spinnerView
    }
    
    func stopLoader() {
        DispatchQueue.main.async {
            self.vSpinner?.removeFromSuperview()
            self.vSpinner = nil
        }
    }
}
