//
//  Sequence+Extension.swift
//  SearchCity
//
//  Created by Adinan Yujaroen on 17/4/2565 BE.
//

import Foundation

extension Sequence {
    func group<U: Hashable>(by key: (Iterator.Element) -> U) -> [U: [Iterator.Element]] {
        return Dictionary.init(grouping: self, by: key)
    }
}
