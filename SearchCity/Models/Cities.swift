//
//  Cities.swift
//  SearchCity
//
//  Created by Adinan Yujaroen on 16/4/2565 BE.
//

import Foundation

struct Cities: Decodable {
    let country: String
    let name: String
    let coord: Coord
    
    struct Coord: Decodable {
        let lon: Double
        let lat: Double
    }
}
