//
//  SplashScreenInteractor.swift
//  SearchCity
//
//  Created by Adinan Yujaroen on 16/4/2565 BE.
//  Copyright (c) 2565 BE ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

protocol SplashScreenInteractorInterface {
    func loadJsonData(request: SplashScreen.LoadData.Request)
    var cities: [String?: [Cities]] { get }
}

class SplashScreenInteractor: SplashScreenInteractorInterface {
    var presenter: SplashScreenPresenterInterface!
    var worker: SearchCityWorker?
    var cities: [String?: [Cities]] = [:]
    
    // MARK: - Business logic
    func loadJsonData(request: SplashScreen.LoadData.Request) {
        worker?.loadJson(completion: { [weak self] response in
            switch response {
            case .success(let cities):
                self?.cities = cities
                self?.presenter.presentLoadJsonDataResult(response: SplashScreen.LoadData.Response())
            case .failure(let error):
                print(error)
            }
        })
    }
}
