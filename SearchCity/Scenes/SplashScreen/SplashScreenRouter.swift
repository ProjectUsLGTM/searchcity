//
//  SplashScreenRouter.swift
//  SearchCity
//
//  Created by Adinan Yujaroen on 16/4/2565 BE.
//  Copyright (c) 2565 BE ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

protocol SplashScreenRouterInterface {
    func navigateToMainPage()
}

class SplashScreenRouter: SplashScreenRouterInterface {
    weak var viewController: SplashScreenViewController!
    
    // MARK: - Navigation
    func navigateToMainPage() {
        let storyBoard : UIStoryboard = UIStoryboard(name: "SearchCityResult", bundle:nil)
        let searchCityResultPage = storyBoard.instantiateViewController(withIdentifier: "searchCityResult") as! SearchCityResultViewController
        searchCityResultPage.interactor.cities = viewController.interactor.cities
        viewController.navigationController?.pushViewController(searchCityResultPage, animated: true)
    }
}
