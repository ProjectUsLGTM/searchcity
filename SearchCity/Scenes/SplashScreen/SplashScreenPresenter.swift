//
//  SplashScreenPresenter.swift
//  SearchCity
//
//  Created by Adinan Yujaroen on 16/4/2565 BE.
//  Copyright (c) 2565 BE ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

protocol SplashScreenPresenterInterface {
    func presentLoadJsonDataResult(response: SplashScreen.LoadData.Response)
}

class SplashScreenPresenter: SplashScreenPresenterInterface {
    weak var viewController: SplashScreenViewControllerInterface!
    
    // MARK: - Presentation logic
    func presentLoadJsonDataResult(response: SplashScreen.LoadData.Response) {
        viewController.displayMainScreen(viewModel: SplashScreen.LoadData.ViewModel())
    }
}
