//
//  SplashScreenViewController.swift
//  SearchCity
//
//  Created by Adinan Yujaroen on 16/4/2565 BE.
//  Copyright (c) 2565 BE ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

protocol SplashScreenViewControllerInterface: AnyObject {
    func displayMainScreen(viewModel: SplashScreen.LoadData.ViewModel)
}

class SplashScreenViewController: UIViewController, SplashScreenViewControllerInterface {
    var interactor: SplashScreenInteractorInterface!
    var router: SplashScreenRouterInterface!
    
    // MARK: - Object lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configure(viewController: self)
    }
    
    // MARK: - Configuration
    
    private func configure(viewController: SplashScreenViewController) {
        let router = SplashScreenRouter()
        router.viewController = viewController
        
        let presenter = SplashScreenPresenter()
        presenter.viewController = viewController
        
        let interactor = SplashScreenInteractor()
        interactor.presenter = presenter
        interactor.worker = SearchCityWorker(store: SearchCityStore())
        
        viewController.interactor = interactor
        viewController.router = router
    }
    
    // MARK: - View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ShareService.shared().showLoader(view: view)
        interactor.loadJsonData(request: SplashScreen.LoadData.Request())
    }
    
    // MARK: - Event handling
    
    // MARK: - Display logic
    func displayMainScreen(viewModel: SplashScreen.LoadData.ViewModel) {
        ShareService.shared().stopLoader()
        router.navigateToMainPage()
    }
    
    // MARK: - Router
    
}
