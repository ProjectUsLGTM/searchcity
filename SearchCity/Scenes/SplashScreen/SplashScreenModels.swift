//
//  SplashScreenModels.swift
//  SearchCity
//
//  Created by Adinan Yujaroen on 16/4/2565 BE.
//  Copyright (c) 2565 BE ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

struct SplashScreen {
  
  struct LoadData {
    
    struct Request {}
    
    struct Response {}
    
    struct ViewModel {}
  }
}
