//
//  SearchCityResultViewController.swift
//  SearchCity
//
//  Created by Adinan Yujaroen on 16/4/2565 BE.
//  Copyright (c) 2565 BE ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit
import MapKit

protocol SearchCityResultViewControllerInterface: AnyObject {
    func displayResultList(viewModel: SearchCityResult.SearchCity.ViewModel)
}

class SearchCityResultViewController: UIViewController, SearchCityResultViewControllerInterface {
    var interactor: SearchCityResultInteractorInterface!
    var router: SearchCityResultRouterInterface!
    var resultList: [Cities] = []
    
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var searchBar: UISearchBar!
    
    // MARK: - Object lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configure(viewController: self)
    }
    
    // MARK: - Configuration
    
    private func configure(viewController: SearchCityResultViewController) {
        let router = SearchCityResultRouter()
        router.viewController = viewController
        
        let presenter = SearchCityResultPresenter()
        presenter.viewController = viewController
        
        let interactor = SearchCityResultInteractor()
        interactor.presenter = presenter
        
        viewController.interactor = interactor
        viewController.router = router
    }
    
    // MARK: - View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setDelegate()
        setTableViewCell()
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    // MARK: - Event handling
    private func setDelegate() {
        searchBar.delegate = self
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    // MARK: - Display logic
    func displayResultList(viewModel: SearchCityResult.SearchCity.ViewModel) {
        resultList = viewModel.resultList
        tableView.reloadData()
    }
    // MARK: - Router
    
}

extension SearchCityResultViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        searchBar.searchTextField.resignFirstResponder()
        
        guard let cell = tableView.cellForRow(at: indexPath) as? SearchCityResultViewCell,
              let latitude = cell.city?.coord.lat,
              let longitude = cell.city?.coord.lon else {
                  return
              }
        // show city location with map
        let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
        let regionSpan =   MKCoordinateRegion(center: coordinates, latitudinalMeters: 1000, longitudinalMeters: 1000)
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)

        mapItem.name = cell.city?.name
        mapItem.openInMaps(launchOptions:[
        MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center)
        ] as [String : Any])
    }
}

extension SearchCityResultViewController: UITableViewDataSource {
    private func setTableViewCell() {
        let nibName = UINib(nibName: SearchCityResultViewCell.identifier, bundle: nil)
        tableView.register(nibName, forCellReuseIdentifier: SearchCityResultViewCell.identifier)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return resultList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: SearchCityResultViewCell.identifier) as? SearchCityResultViewCell else {
            return UITableViewCell()
        }
        cell.updateUI(city: resultList[indexPath.row])
        return cell
    }
}

extension SearchCityResultViewController: UISearchBarDelegate {
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.searchTextField.resignFirstResponder()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.searchTextField.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        let request = SearchCityResult.SearchCity.Request(inputText: searchText)
        interactor.searchCity(request: request)
    }
}
