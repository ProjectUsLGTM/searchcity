//
//  SearchCityResultModels.swift
//  SearchCity
//
//  Created by Adinan Yujaroen on 16/4/2565 BE.
//  Copyright (c) 2565 BE ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

struct SearchCityResult {
  
  struct SearchCity {
    
    struct Request {
        let inputText: String
    }
    
    struct Response {
        let resultList: [Cities]
    }
    
    struct ViewModel {
        let resultList: [Cities]
    }
  }
}
