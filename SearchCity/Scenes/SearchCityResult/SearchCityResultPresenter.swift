//
//  SearchCityResultPresenter.swift
//  SearchCity
//
//  Created by Adinan Yujaroen on 16/4/2565 BE.
//  Copyright (c) 2565 BE ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

protocol SearchCityResultPresenterInterface {
    func presentResultList(response: SearchCityResult.SearchCity.Response)
}

class SearchCityResultPresenter: SearchCityResultPresenterInterface {
    weak var viewController: SearchCityResultViewControllerInterface!
    
    // MARK: - Presentation logic
    func presentResultList(response: SearchCityResult.SearchCity.Response) {
        let viewModel = SearchCityResult.SearchCity.ViewModel(resultList: response.resultList)
        viewController.displayResultList(viewModel: viewModel)
    }
}
