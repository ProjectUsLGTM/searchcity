//
//  SearchCityResultInteractor.swift
//  SearchCity
//
//  Created by Adinan Yujaroen on 16/4/2565 BE.
//  Copyright (c) 2565 BE ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

protocol SearchCityResultInteractorInterface {
    var cities: [String? : [Cities]] { get set }
    func searchCity(request: SearchCityResult.SearchCity.Request)
}

class SearchCityResultInteractor: SearchCityResultInteractorInterface {
    
    var presenter: SearchCityResultPresenterInterface!
    
    var cities: [String? : [Cities]] = [:]
    
    // MARK: - Business logic
    func searchCity(request: SearchCityResult.SearchCity.Request) {
        guard let searchGroup: String = request.inputText.first?.lowercased(),
              let resultList = cities[searchGroup]?.filter({ $0.name.lowercased().hasPrefix(request.inputText.lowercased()) }) else {
            let response = SearchCityResult.SearchCity.Response(resultList: [])
            presenter.presentResultList(response: response)
            return
        }
        let response = SearchCityResult.SearchCity.Response(resultList: resultList)
        presenter.presentResultList(response: response)
    }
}
