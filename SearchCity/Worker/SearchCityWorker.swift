//
//  SearchCityWorker.swift
//  SearchCity
//
//  Created by Adinan Yujaroen on 16/4/2565 BE.
//

import Foundation

class SearchCityWorker {
    var searchCityStore: SearchCityStoreProtocol
    
    init(store: SearchCityStoreProtocol) {
        self.searchCityStore = store
    }
    
    func loadJson(completion: @escaping (Result<[String?: [Cities]], ResponseError>) -> Void) {
        searchCityStore.loadJson { [weak self] result in
            switch result {
            case .success(let cities):
                guard let resultData = self?.manageData(cities: cities) else {
                    completion(.failure(.manageDataError))
                    return
                }
                completion(.success(resultData))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    // sort data and group data by first charecter of city name
    private func manageData(cities: [Cities]) -> [String?: [Cities]] {
        let sortedData = cities.sorted(by: { $0.name.lowercased() < $1.name.lowercased() })
        let groupData = sortedData.group(by: { $0.name.first?.lowercased() })
        return groupData
    }
}
