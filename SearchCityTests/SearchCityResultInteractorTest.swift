//
//  SearchCityResultInteractorTest.swift
//  SearchCityTests
//
//  Created by Adinan Yujaroen on 17/4/2565 BE.
//

import XCTest
@testable import SearchCity

class SearchCityResultInteractorTest: XCTestCase {
    
    var interactor: SearchCityResultInteractor!
    var outputSpy: SearchCityResultPresenterSpy!
    
    class SearchCityResultPresenterSpy: SearchCityResultPresenterInterface {
        var presentResultListCalled: Bool = false
        var presentResultListResponse: SearchCityResult.SearchCity.Response!
        
        func presentResultList(response: SearchCityResult.SearchCity.Response) {
            presentResultListCalled = true
            presentResultListResponse = response
        }
    }

    override func setUp() {
        super.setUp()
        interactor = SearchCityResultInteractor()
        outputSpy = SearchCityResultPresenterSpy()
        interactor.presenter = outputSpy
        // data that already managed
        interactor.cities = ["a": [Cities(country: "UA", name: "ALfortville", coord: Cities.Coord(lon: 34.283333, lat: 44.549999)),
                                   Cities(country: "UA", name: "Arrondissement", coord: Cities.Coord(lon: 34.283333, lat: 44.549999)),
                                   Cities(country: "UA", name: "Auch", coord: Cities.Coord(lon: 34.283333, lat: 44.549999))],
                             "b": [Cities(country: "OM", name: "Bidbid", coord: Cities.Coord(lon: 34.283333, lat: 44.549999)),
                                   Cities(country: "DE", name: "Brüggen", coord: Cities.Coord(lon: 34.283333, lat: 44.549999))],
                             "h": [Cities(country: "UA", name: "Holubynka", coord: Cities.Coord(lon: 33.900002, lat: 44.599998)),
                                   Cities(country: "UA", name: "Hurzuf", coord: Cities.Coord(lon: 34.283333, lat: 44.549999))],
                             "g": [Cities(country: "IT", name: "Grandate", coord: Cities.Coord(lon: 9.05784, lat: 45.775181))],
                             "k": [Cities(country: "UA", name: "Ka Bang", coord: Cities.Coord(lon: 34.283333, lat: 44.549999)),
                                   Cities(country: "UA", name: "Ketovo", coord: Cities.Coord(lon: 34.283333, lat: 44.549999)),
                                   Cities(country: "UA", name: "Kolito", coord: Cities.Coord(lon: 34.283333, lat: 44.549999))],
                             "w": [Cities(country: "UA", name: "Wattrelos", coord: Cities.Coord(lon: 34.283333, lat: 44.549999))],
                             "s": [Cities(country: "UA", name: "Sèvres", coord: Cities.Coord(lon: 34.283333, lat: 44.549999))]
        ]
    }

    func testSearchCityCaseNotFound() {
        // Given
        let request = SearchCityResult.SearchCity.Request(inputText: "EE")
        
        // when
        interactor.searchCity(request: request)
        
        // Then
        XCTAssertTrue(outputSpy.presentResultListCalled, "searchCity() should call presentResultList()")
        XCTAssertTrue(outputSpy.presentResultListResponse.resultList.isEmpty, "resultList should be empty")
    }
    
    func testSearchCityCaseInputMatchWithData() {
        // Given
        let request = SearchCityResult.SearchCity.Request(inputText: "AL")
        
        // when
        interactor.searchCity(request: request)
        
        // Then
        XCTAssertTrue(outputSpy.presentResultListCalled, "searchCity() should call presentResultList()")
        XCTAssertEqual(outputSpy.presentResultListResponse.resultList.first?.name, "ALfortville")
    }
    
    func testSearchCityCaseInputLowerCaseAndMatchWithData() {
        // Given
        let request = SearchCityResult.SearchCity.Request(inputText: "al")
        
        // when
        interactor.searchCity(request: request)
        
        // Then
        XCTAssertTrue(outputSpy.presentResultListCalled, "searchCity() should call presentResultList()")
        XCTAssertEqual(outputSpy.presentResultListResponse.resultList.first?.name, "ALfortville")
    }
    
    func testSearchCityCaseInputWhenInputWrongFormat() {
        // Given
        let request = SearchCityResult.SearchCity.Request(inputText: "?.")
        
        // when
        interactor.searchCity(request: request)
        
        // Then
        XCTAssertTrue(outputSpy.presentResultListCalled, "searchCity() should call presentResultList()")
        XCTAssertTrue(outputSpy.presentResultListResponse.resultList.isEmpty, "resultList should be empty")
    }
    
    func testSearchCityCaseInputWhenGetCorrectOrder() {
        // Given
        let request = SearchCityResult.SearchCity.Request(inputText: "k")
        
        // when
        interactor.searchCity(request: request)
        
        // Then
        XCTAssertTrue(outputSpy.presentResultListCalled, "searchCity() should call presentResultList()")
        XCTAssertEqual(outputSpy.presentResultListResponse.resultList.first?.name, "Ka Bang")
        XCTAssertEqual(outputSpy.presentResultListResponse.resultList[1].name, "Ketovo")
        XCTAssertEqual(outputSpy.presentResultListResponse.resultList[2].name, "Kolito")
    }
}
